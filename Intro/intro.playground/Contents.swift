import Foundation
//: First task
//: Data types
print("Int data types", terminator: "\n\n")

print("Int.max = \(Int.max)")
print("Int.min = \(Int.min)")
print("Int64.max = \(Int64.max)")
print("Int64.min = \(Int64.min)")
print("Int32.max = \(Int32.max)")
print("Int32.min = \(Int32.min)")
print("Int16.max = \(Int16.max)")
print("Int16.min = \(Int16.min)")
print("Int8.max = \(Int8.max)")
print("Int8.min = \(Int8.min)", terminator: "\n\n")

print("Maximum Double = \(Double.greatestFiniteMagnitude)")
print("Double -> 0 = \(Double.leastNonzeroMagnitude)", terminator: "\n\n")

print("Maximum Float = \(Float.greatestFiniteMagnitude)")
print("Float -> 0 = \(Float.leastNonzeroMagnitude)", terminator: "\n\n")

print("Maximum Decimal = \(Decimal.greatestFiniteMagnitude)", terminator: "\n\n")
print("Maximum Decimal = \(Decimal.leastNonzeroMagnitude)", terminator: "\n\n")

//: Memory layout
print("Byte size of Int - \(MemoryLayout<Int>.size)")
print("Byte size of Int64 - \(MemoryLayout<Int64>.size)")
print("Byte size of Int32 - \(MemoryLayout<Int32>.size)")
print("Byte size of Int16 - \(MemoryLayout<Int16>.size)")
print("Byte size of Int - \(MemoryLayout<UInt8>.size)", terminator: "\n\n")

print("Byte size of Double - \(MemoryLayout<Double>.size)")
print("Byte size of Float - \(MemoryLayout<Float>.size)")
print("Byte size of Decimal - \(MemoryLayout<Decimal>.size)", terminator: "\n\n")

//: Memory layout optionals
print("Byte size of optional Int - \(MemoryLayout<Int?>.size)")
print("Byte size of optional Int64 - \(MemoryLayout<Int64?>.size)")
print("Byte size of optional Int32 - \(MemoryLayout<Int32?>.size)")
print("Byte size of optional Int16 - \(MemoryLayout<Int16?>.size)")
print("Byte size of optional Int - \(MemoryLayout<Int8?>.size)", terminator: "\n\n")

print("Byte size of optional Double - \(MemoryLayout<Double?>.size)")
print("Byte size of optional Float - \(MemoryLayout<Float?>.size)")
print("Byte size of optional Decimal - \(MemoryLayout<Decimal?>.size)",terminator: "\n\n\n\n")



//:Second task


let first : Int = 10
let second = 10.5
let third : Float = 11.5


let resultInt = Int(Double(first) + second + Double(third))
let resultDouble = Double(first) + second + Double(third)
let resultFloat = Float(first) + Float(second) + third

if (Double(resultInt) <= resultDouble){
    print(resultDouble, terminator: "\n\n")
}
else{
    print(resultInt, terminator: "\n\n")
}



struct Student{
    var age : Int
    var groupNumber : UInt16 //Чаще всего НЕ следует использовать такие типы данных, экономия ресурсов при сегодняшних вычислительных мощностях, почти не влияет на производительность(не берем в расчет ресурсоемкие задачи)
    //Увеличение читаемости и понимания кода > экономия 3 байт памяти на одну переменную
    var firstName : String
    var secondName : String
    
    func getInformation() {
        print(firstName, secondName)
        print("Age is \(age)")
        print("Group is \(groupNumber)")
        
    }
}

var me = Student(age: 24, groupNumber: 7391, firstName: "Pavel", secondName: "Chvyrov")

me.getInformation()


